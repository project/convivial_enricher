<?php

namespace Drupal\convivial_enricher;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * EndpointProcessor service.
 *
 * Takes a plugin id and data parameter, then hands over
 * to the plugin.
 */
interface EnricherHelperInterface {

  /**
   * Take the incoming data, process it.
   *
   * The data parameter is encoded, so we decoded it and pass the id (token)
   * to the Datasource plugin for look up and retrieval of more data.
   *
   * Finally, encode the return_to value which was decoded from
   * the data parameter into a redirectResponse and pass that response back
   * to the controller.
   *
   * This is designed to be called from
   * \Drupal\convivial_enricher\Controller\EnricherController::build
   *
   * @param mixed $data
   *   The data parameter passed in from our controller.
   * @param string $enricher_id
   *   The enricher id to process this request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to the return_to encoded in the $data.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function processIncomingData($data, string $enricher_id): RedirectResponse;

}
