<?php

namespace Drupal\convivial_enricher;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Enricher Datasource plugin manager.
 */
class EnricherDatasourceManager extends DefaultPluginManager {

  /**
   * Constructs EnricherDatasourceManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/EnricherDatasource',
      $namespaces,
      $module_handler,
      'Drupal\convivial_enricher\EnricherDatasourceInterface',
      'Drupal\convivial_enricher\Annotation\EnricherDatasource'
    );
    $this->alterInfo('convivial_enricher_datasource_info');
    $this->setCacheBackend($cache_backend, 'convivial_enricher_datasource_plugins');
  }

}
