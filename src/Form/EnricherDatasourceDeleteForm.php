<?php

namespace Drupal\convivial_enricher\Form;

use Drupal\convivial_enricher\EnricherInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for deleting an enricher datasource.
 *
 * @internal
 */
class EnricherDatasourceDeleteForm extends ConfirmFormBase {

  /**
   * The enricher containing the datasource to be deleted.
   *
   * @var \Drupal\convivial_enricher\EnricherInterface
   */
  protected $enricher;

  /**
   * The enricher datasource to be deleted.
   *
   * @var \Drupal\convivial_enricher\EnricherDatasourceInterface
   */
  protected $enricherDatasource;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the @datasource from the %enricher?', [
      '%enricher' => $this->enricher->label(),
      '@datasource' => $this->enricherDatasource->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->enricher->toUrl('edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'convivial_enricher_datasource_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EnricherInterface $enricher = NULL, $enricher_datasource = NULL) {
    $this->enricher = $enricher;
    $this->enricherDatasource = $this->enricher->getDatasource($enricher_datasource);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->enricher->deleteEnricherDatasource($this->enricherDatasource);
    $this->messenger()->addStatus($this->t('The datasource %name has been deleted.', ['%name' => $this->enricherDatasource->label()]));
    $form_state->setRedirectUrl($this->enricher->toUrl('edit-form'));
  }

}
