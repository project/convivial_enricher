<?php

namespace Drupal\convivial_enricher\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Controller for enricher addition forms.
 *
 * @internal
 */
class EnricherAddForm extends EnricherFormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('Enricher %name was created.', ['%name' => $this->entity->label()]));
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Create new enricher');

    return $actions;
  }

}
