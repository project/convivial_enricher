<?php

namespace Drupal\convivial_enricher\Form;

use Drupal\convivial_enricher\EnricherDatasourceManager;
use Drupal\convivial_enricher\EnricherInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an add form for enricher datasources.
 *
 * @internal
 */
class EnricherDatasourceAddForm extends EnricherDatasourceFormBase {

  /**
   * The enricher datasource manager.
   *
   * @var \Drupal\convivial_enricher\EnricherDatasourceManager
   */
  protected $datasourceManager;

  /**
   * Constructs a new EnricherDatasourceAddForm.
   *
   * @param \Drupal\convivial_enricher\EnricherDatasourceManager $datasource_manager
   *   The enricher datasource manager.
   */
  public function __construct(EnricherDatasourceManager $datasource_manager) {
    $this->datasourceManager = $datasource_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('plugin.manager.enricher.datasource')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EnricherInterface $enricher = NULL, $enricher_datasource = NULL) {
    $form = parent::buildForm($form, $form_state, $enricher, $enricher_datasource);

    $form['#title'] = $this->t('Add %label datasource', ['%label' => $this->enricherDatasource->label()]);
    $form['actions']['submit']['#value'] = $this->t('Add datasource');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEnricherDatasource($enricher_datasource) {
    $enricher_datasource = $this->datasourceManager->createInstance($enricher_datasource);
    // Set the initial weight so this datasource comes last.
    $enricher_datasource->setWeight(count($this->enricher->getDatasources()));
    return $enricher_datasource;
  }

}
