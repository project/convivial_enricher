<?php

namespace Drupal\convivial_enricher\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form for enricher add and edit forms.
 */
abstract class EnricherFormBase extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\convivial_enricher\EnricherInterface
   */
  protected $entity;

  /**
   * The route building service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * The enricher entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $enricherStorage;

  /**
   * Constructs a base class for enricher add and edit forms.
   *
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The route building service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $enricher_storage
   *   The enricher entity storage.
   */
  public function __construct(RouteBuilderInterface $route_builder, EntityStorageInterface $enricher_storage) {
    $this->routeBuilder = $route_builder;
    $this->enricherStorage = $enricher_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.builder'),
    $container->get('entity_type.manager')->getStorage('enricher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enricher name'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['name'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this->enricherStorage, 'load'],
      ],
      '#default_value' => $this->entity->id(),
      '#required' => TRUE,
    ];

    $help = nl2br($this->t("Set the endpoint path for this datasource.
Example: /active_campaign
If your referral URL is /active_campaign/&lt;token&gt;/return/to/path then enter /active_campaign here."));

    $form['endpoint_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint'),
      '#maxlength' => 255,
      '#default_value' => $this->prependLeadingSlashToString($this->entity->get('endpoint_path') ?? ''),
      '#description' => $help,
      '#required' => TRUE,
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the enricher.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $this->validateEndpointPathIsUnique($form["id"]["#default_value"] ?? '', $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * Here we intercept the form submission and do some tidy ups
   * on our form state values before the parent class prepares the entity
   * for saving.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->removeLeadingSlashFromFormStateEndpointValue($form_state);
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('edit-form'));
  }

  /**
   * Given a string, remove the leading slash and return the string.
   *
   * @param string $string
   *   The subject string to strip the slash from.
   *
   * @return string
   *   The string with the leading slash removed.
   */
  protected function stripLeadingSlashFromString(string $string): string {
    return ltrim($string, '/');
  }

  /**
   * Given a string, prepend a slash and return the string.
   *
   * @param string $string
   *   The subject string to strip the slash from.
   *
   * @return string
   *   The string with the leading slash removed.
   */
  protected function prependLeadingSlashToString(string $string): string {
    return '/' . $string;
  }

  /**
   * Remove the leading slash from the endpoint value in our form_start.
   *
   * By stripping the leading slash before the parent class prepares and saves
   * this entity our form can accept and save paths with forward slashes, while
   * our system continues to function using paths without that slash.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state of this form.
   */
  protected function removeLeadingSlashFromFormStateEndpointValue(FormStateInterface $form_state): void {
    $form_state->setValue('endpoint_path', $this->stripLeadingSlashFromString($form_state->getValue('endpoint_path')));
  }

  /**
   * Validate that no Enrichers have the same endpoint as this Enricher ID.
   *
   * @param string $enricher_id
   *   The ID (machine name) of the Enricher entity associated with this form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FromState of this form as passed in from validateForm().
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function validateEndpointPathIsUnique(string $enricher_id, FormStateInterface $form_state): void {
    if (!empty($enricher_id)) {
      $enricher_entities = $this->loadEnricherConfigEntities();
      foreach ($enricher_entities as $enricher_entity) {
        // If we're editing the form, then we've just loaded the same entity
        // that we're validating. Let's skip validating against ourselves.
        if ($enricher_entity->id() != $enricher_id) {
          // Endpoint paths are saved with the leading slash stripped, so we
          // match remove it when matching.
          // If the loaded enricher has the same path as the one we're saving
          // then fail validation.
          if ($enricher_entity->get('endpoint_path') == $this->stripLeadingSlashFromString($form_state->getValue('endpoint_path'))) {
            $form_state->setErrorByName('endpoint_path', $this->t('That endpoint path entered is already used by another enricher entity, please try another.'));
          }
        }
      }
    }
  }

  /**
   * Load all the enricher config entities.
   *
   * @return \Drupal\convivial_enricher\EnricherInterface[]
   *   An array of Enricher config entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadEnricherConfigEntities(): array {
    $entity_storage = $this->entityTypeManager->getStorage('enricher');
    return $entity_storage->loadMultiple();
  }

}
