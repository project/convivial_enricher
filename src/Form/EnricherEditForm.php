<?php

namespace Drupal\convivial_enricher\Form;

use Drupal\Component\Utility\Unicode;
use Drupal\convivial_enricher\EnricherDatasourceManager;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for enricher edit form.
 *
 * @internal
 */
class EnricherEditForm extends EnricherFormBase {

  /**
   * The enricher datasource manager service.
   *
   * @var \Drupal\convivial_enricher\EnricherDatasourceManager
   */
  protected $enricherDatasourceManager;

  /**
   * Constructs an EnricherEditForm object.
   *
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The route building service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $enricher_storage
   *   The enricher entity storage.
   * @param \Drupal\convivial_enricher\EnricherDatasourceManager $enricher_datasource_manager
   *   The enricher datasource manager service.
   */
  public function __construct(RouteBuilderInterface $route_builder, EntityStorageInterface $enricher_storage, EnricherDatasourceManager $enricher_datasource_manager) {
    parent::__construct($route_builder, $enricher_storage);
    $this->enricherDatasourceManager = $enricher_datasource_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.builder'),
      $container->get('entity_type.manager')->getStorage('enricher'),
      $container->get('plugin.manager.enricher.datasource')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $user_input = $form_state->getUserInput();
    $form['#title'] = $this->t('Edit enricher %name', ['%name' => $this->entity->label()]);
    $form['#tree'] = TRUE;

    // Build the list of existing enricher datasources for this enricher.
    $form['datasources'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Datasource'),
        $this->t('Weight'),
        $this->t('Operations'),
      ],
      '#tabledrag' => [
      [
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => 'enricher-datasource-order-weight',
      ],
      ],
      '#attributes' => [
        'id' => 'enricher-datasources',
      ],
      '#empty' => $this->t('There are currently no datasources in this enricher. Add one by selecting an option below.'),
      // Render datasources below parent elements.
      '#weight' => 5,
    ];

    foreach ($this->entity->getDatasources() as $datasource) {
      $key = $datasource->getUuid();
      $form['datasources'][$key]['#attributes']['class'][] = 'draggable';
      $form['datasources'][$key]['#weight'] = isset($user_input['datasources']) ? $user_input['datasources'][$key]['weight'] : NULL;
      $form['datasources'][$key]['datasource'] = [
        '#tree' => FALSE,
        'settings' => [
          'label' => [
            '#plain_text' => $datasource->label(),
          ],
        ],
      ];

      $summary = $datasource->getSummary();

      if (!empty($summary)) {
        $summary['#prefix'] = ' ';
        $form['datasources'][$key]['datasource']['settings']['summary'] = $summary;
      }

      $form['datasources'][$key]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $datasource->label()]),
        '#title_display' => 'invisible',
        '#default_value' => $datasource->getWeight(),
        '#attributes' => [
          'class' => ['enricher-datasource-order-weight'],
        ],
      ];

      $links = [];
      $links['edit'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('convivial_enricher.datasource_edit_form', [
          'enricher' => $this->entity->id(),
          'enricher_datasource' => $key,
        ]),
      ];
      $links['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('convivial_enricher.datasource_delete', [
          'enricher' => $this->entity->id(),
          'enricher_datasource' => $key,
        ]),
      ];
      $form['datasources'][$key]['operations'] = [
        '#type' => 'operations',
        '#links' => $links,
      ];
    }

    // Build the new enricher datasource addition form and
    // add it to the datasource list.
    $new_datasource_options = [];
    $datasources = $this->enricherDatasourceManager->getDefinitions();
    uasort($datasources, function ($a, $b) {
      return Unicode::strcasecmp($a['label'], $b['label']);
    });
    foreach ($datasources as $datasource => $definition) {
      $new_datasource_options[$datasource] = $definition['label'];
    }
    $form['datasources']['new'] = [
      '#tree' => FALSE,
      '#weight' => $user_input['weight'] ?? NULL,
      '#attributes' => ['class' => ['draggable']],
    ];
    $form['datasources']['new']['datasource'] = [
      'settings' => [
        'new' => [
          '#type' => 'select',
          '#title' => $this->t('Datasource'),
          '#title_display' => 'invisible',
          '#options' => $new_datasource_options,
          '#empty_option' => $this->t('Select a new datasource'),
        ],
        [
          'add' => [
            '#type' => 'submit',
            '#value' => $this->t('Add'),
            '#validate' => ['::datasourceValidate'],
            '#submit' => ['::submitForm', '::datasourceSave'],
          ],
        ],
      ],
      '#prefix' => '<div class="enricher-new">',
      '#suffix' => '</div>',
    ];

    $form['datasources']['new']['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight for new datasource'),
      '#title_display' => 'invisible',
      '#default_value' => count($this->entity->getDatasources()) + 1,
      '#attributes' => ['class' => ['enricher-datasource-order-weight']],
    ];
    $form['datasources']['new']['operations'] = [
      'settings' => [],
    ];

    return parent::form($form, $form_state);
  }

  /**
   * Validate handler for enricher datasource.
   */
  public function datasourceValidate($form, FormStateInterface $form_state) {
    if (!$form_state->getValue('new')) {
      $form_state->setErrorByName('new', $this->t('Select a datasource to add.'));
    }
  }

  /**
   * Submit handler for enricher datasource.
   */
  public function datasourceSave($form, FormStateInterface $form_state) {
    $this->save($form, $form_state);

    // Load the configuration form for this option.
    $form_state->setRedirect(
    'convivial_enricher.datasource_add_form',
                                [
                                  'enricher' => $this->entity->id(),
                                  'enricher_datasource' => $form_state->getValue('new'),
                                ],
                                ['query' => ['weight' => $form_state->getValue('weight')]]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Update enricher datasource weights.
    if (!$form_state->isValueEmpty('datasources')) {
      $this->updateDatasourceWeights($form_state->getValue('datasources'));
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    $this->messenger()->addStatus($this->t('Changes to the enricher have been saved.'));
  }

  /**
   * Updates enricher datasource weights.
   *
   * @param array $datasources
   *   Associative array with datasources having datasource uuid as keys and
   *   array with datasource data as values.
   */
  protected function updateDatasourceWeights(array $datasources) {
    foreach ($datasources as $uuid => $datasource_data) {
      if ($this->entity->getDatasources()->has($uuid)) {
        $this->entity->getDatasource($uuid)->setWeight($datasource_data['weight']);
      }
    }
  }

}
