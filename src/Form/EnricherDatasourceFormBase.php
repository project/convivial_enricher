<?php

namespace Drupal\convivial_enricher\Form;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\convivial_enricher\EnricherDatasourceInterface;
use Drupal\convivial_enricher\EnricherInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a base form for enricher datasources.
 */
abstract class EnricherDatasourceFormBase extends FormBase {

  /**
   * The enricher.
   *
   * @var \Drupal\convivial_enricher\EnricherInterface
   */
  protected $enricher;

  /**
   * The enricher datasource.
   *
   * @var \Drupal\convivial_enricher\EnricherDatasourceInterface
   */
  protected $enricherDatasource;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'enricher_datasource_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\convivial_enricher\EnricherInterface $enricher
   *   The enricher.
   * @param string $enricher_datasource
   *   The enricher datasource ID.
   *
   * @return array
   *   The form structure.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function buildForm(array $form, FormStateInterface $form_state, EnricherInterface $enricher = NULL, $enricher_datasource = NULL) {
    $this->enricher = $enricher;
    try {
      $this->enricherDatasource = $this->prepareEnricherDatasource($enricher_datasource);
    }
    catch (PluginNotFoundException $e) {
      throw new NotFoundHttpException("Invalid datasource id: '$enricher_datasource'.");
    }
    $request = $this->getRequest();

    if (!($this->enricherDatasource instanceof EnricherDatasourceInterface)) {
      throw new NotFoundHttpException();
    }

    $form['uuid'] = [
      '#type' => 'value',
      '#value' => $this->enricherDatasource->getUuid(),
    ];
    $form['id'] = [
      '#type' => 'value',
      '#value' => $this->enricherDatasource->getPluginId(),
    ];

    $form['settings'] = [];
    $subform_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    $form['settings'] = $this->enricherDatasource->buildConfigurationForm($form['settings'], $subform_state);
    $form['settings']['#tree'] = TRUE;

    // Check URL for a weight,then enricher datasource,otherwise use default.
    $form['weight'] = [
      '#type' => 'hidden',
      '#value' => $request->query->has('weight') ? (int) $request->query->get('weight') : $this->enricherDatasource->getWeight(),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $this->enricher->toUrl('edit-form'),
      '#attributes' => ['class' => ['button']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // The enricher datasource configuration is stored in the 'settings' key in
    // the form, pass that through for validation.
    $this->enricherDatasource->validateConfigurationForm($form['settings'], SubformState::createForSubform($form['settings'], $form, $form_state));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();

    // The enricher datasource configuration is stored in the 'settings' key in
    // the form, pass that through for submission.
    $this->enricherDatasource->submitConfigurationForm($form['settings'], SubformState::createForSubform($form['settings'], $form, $form_state));

    $this->enricherDatasource->setWeight($form_state->getValue('weight'));
    if (!$this->enricherDatasource->getUuid()) {
      $this->enricher->addEnricherDatasource($this->enricherDatasource->getConfiguration());
    }
    $this->enricher->save();

    $this->messenger()->addStatus($this->t('The enricher datasource was successfully applied.'));
    $form_state->setRedirectUrl($this->enricher->toUrl('edit-form'));
  }

  /**
   * Converts an enricher datasource ID into an object.
   *
   * @param string $enricher_datasource
   *   The enricher datasource ID.
   *
   * @return \Drupal\convivial_enricher\EnricherDatasourceInterface
   *   The enricher datasource object.
   */
  abstract protected function prepareEnricherDatasource($enricher_datasource);

}
