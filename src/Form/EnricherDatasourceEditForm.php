<?php

namespace Drupal\convivial_enricher\Form;

use Drupal\convivial_enricher\EnricherInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an edit form for enricher datasources.
 *
 * @internal
 */
class EnricherDatasourceEditForm extends EnricherDatasourceFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EnricherInterface $enricher = NULL, $enricher_datasource = NULL) {
    $form = parent::buildForm($form, $form_state, $enricher, $enricher_datasource);

    $form['#title'] = $this->t('Edit %label datasource', ['%label' => $this->enricherDatasource->label()]);
    $form['actions']['submit']['#value'] = $this->t('Update datasource');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEnricherDatasource($enricher_datasource) {
    return $this->enricher->getDatasource($enricher_datasource);
  }

}
