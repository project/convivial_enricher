<?php

namespace Drupal\convivial_enricher\Plugin\EnricherDatasource;

use Drupal\convivial_enricher\EnricherDatasourceBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the datasource.
 *
 * @EnricherDatasource(
 *   id = "dummy",
 *   label = @Translation("Dummy datasource"),
 *   description = @Translation("Useful for testing purposes."),
 * )
 */
class DummyEnricherDatasource extends EnricherDatasourceBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'dummy_title' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['dummy_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dummy Textfield'),
      '#description' => $this->t('Dummy Textfield'),
      '#default_value' => $this->configuration['dummy_title'] ?? '',
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['dummy_title'] = $form_state->getValue('dummy_title');
  }

  /**
   * {@inheritdoc}
   */
  public function fetchAndProcessData($key) {
    return $key;
  }

  /**
   * {@inheritdoc}
   */
  public function processIncomingPath(&$path, string $endpoint_path) {
    return $path;
  }

}
