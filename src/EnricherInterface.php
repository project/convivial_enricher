<?php

namespace Drupal\convivial_enricher;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Enricher config entity.
 */
interface EnricherInterface extends ConfigEntityInterface {

  /**
   * Returns the enricher.
   *
   * @return string
   *   The name of the enricher.
   */
  public function getName();

  /**
   * Sets the name of the enricher.
   *
   * @param string $name
   *   The name of the enricher.
   *
   * @return $this
   *   The class instance this method is called on.
   */
  public function setName($name);

  /**
   * Returns a specific datasource.
   *
   * @param string $datasource
   *   The datasource ID.
   *
   * @return \Drupal\convivial_enricher\EnricherDatasourceInterface
   *   The convivial enricher datasource object.
   */
  public function getDatasource($datasource);

  /**
   * Returns the enricher datasources for this enricher.
   *
   * @return \Drupal\convivial_enricher\EnricherDatasourcePluginCollection|\Drupal\convivial_enricher\EnricherDatasourceInterface[]
   *   The convivial enricher datasource plugin collection.
   */
  public function getDatasources();

  /**
   * Saves a datasource for this enricher.
   *
   * @param array $configuration
   *   An array of enricher datasource configuration.
   *
   * @return string
   *   The enricher datasource ID.
   */
  public function addEnricherDatasource(array $configuration);

  /**
   * Deletes a datasource from this enricher.
   *
   * @param \Drupal\convivial_enricher\EnricherDatasourceInterface $datasource
   *   The enricher datasource object.
   *
   * @return $this
   */
  public function deleteEnricherDatasource(EnricherDatasourceInterface $datasource);

}
