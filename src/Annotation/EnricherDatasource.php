<?php

namespace Drupal\convivial_enricher\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines EnricherDatasource annotation object.
 *
 * @Annotation
 */
class EnricherDatasource extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
