<?php

namespace Drupal\convivial_enricher\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Enricher route subscriber.
 *
 * This subscriber is responsible for generating the routes for each enricher
 * config entity's endpoint_path.
 */
class EnricherEndpointRouteSubscriber extends RouteSubscriberBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * Construct a new EnricherEndpointRouteSubscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service for loading the config entities.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    $enricher_entities = $this->getEnabledEnricherConfigEntities();

    foreach ($enricher_entities as $enricher) {

      $route = new Route('/' . $enricher->get('endpoint_path') . '/{data}');

      $route->setDefaults([
        '_controller' => '\Drupal\convivial_enricher\Controller\EnricherController::build',
        'enricher_id' => $enricher->id(),
        'data' => 'request_keys',
      ]);

      $route->setRequirements([
        '_access' => 'TRUE',
      ]);

      $route->setOptions([
        'parameters' => [
          'enricher_id',
          'data',
          'nocache' => TRUE,
        ],
      ]);

      $collection->add('enricher_endpoint.endpoint.' . $enricher->id(), $route);
    }

  }

  /**
   * Load EnricherConfigEntities that are enabled.
   *
   * @return \Drupal\convivial_enricher\EnricherInterface[]
   *   An array of EnricherConfigEntities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getEnabledEnricherConfigEntities(): array {
    $entity_storage = $this->entityTypeManager->getStorage('enricher');

    $enabled_enricher_ids = $entity_storage->getQuery()
      ->condition('status', 1)
      ->accessCheck(TRUE)
      ->execute();

    return $entity_storage->loadMultiple($enabled_enricher_ids);
  }

}
