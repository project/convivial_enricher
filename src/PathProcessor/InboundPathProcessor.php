<?php

namespace Drupal\convivial_enricher\PathProcessor;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Standard drupal path processor.
 *
 * This class intercepts the incoming path and hands it over to our
 * dedicated endpoint processor for performing endpoint related processes.
 */
class InboundPathProcessor implements InboundPathProcessorInterface {

  /**
   * Entity type manager for loading enricher configuration entities.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * Construct an EncricherPathProcessor class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Entity type manager for loading enricher configuration entities.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {

    $enrichers = $this->loadEnricherConfigEntities();
    foreach ($enrichers as $enricher) {
      if ($enricher->get('datasources')) {
        $datasources = $enricher->get('datasources');
        foreach ($datasources as $key => $datasource) {
          $enricher->getDatasource($key)->processIncomingPath($path, $enricher->get('endpoint_path'));
        }
      }
    }
    return $path;
  }

  /**
   * Load the enricher config entities.
   *
   * @return \Drupal\convivial_enricher\EnricherInterface[]
   *   An array of entity objects indexed by their IDs. Returns an empty array
   *   if no matching entities are found.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function loadEnricherConfigEntities(): array {
    $entity_storage = $this->entityTypeManager->getStorage('enricher');
    $enabled_enricher_ids = $entity_storage->getQuery()
      ->condition('status', 1)
      ->accessCheck(TRUE)
      ->execute();
    return $entity_storage->loadMultiple($enabled_enricher_ids);
  }

}
