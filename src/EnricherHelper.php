<?php

namespace Drupal\convivial_enricher;

use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * EnricherHelper service.
 *
 * This EnricherHelper is designed to be called from our Enricher Controller.
 * It takes a plugin id and data parameter, then hands over to the plugin.
 */
class EnricherHelper implements EnricherHelperInterface {

  /**
   * Entity type manager service.
   *
   * Used for loading Enricher config entities.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs an EnricherHelper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   An enricher entity we can call its plugin and associated data processor.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager->getStorage('enricher');
  }

  /**
   * {@inheritdoc}
   */
  public function processIncomingData($data, string $enricher_id): RedirectResponse {

    $data_decoded = $this->dataToArray($data);
    if ($data_decoded['return_to'] && !empty($data_decoded['return_to'])) {
      $response = new RedirectResponse($data_decoded['return_to']);
      $response->setStatusCode(Response::HTTP_TEMPORARY_REDIRECT);
      /** @var \Drupal\convivial_enricher\EnricherInterface $enricher */
      $enricher = $this->entityTypeManager->load($enricher_id);
      if ($enricher->get('datasources')) {
        $datasources_output = [];
        $token = $this->dataToArray($data)['token'];
        $datasources = $enricher->get('datasources');
        foreach ($datasources as $key => $datasource) {
          /** @var \Drupal\convivial_enricher\EnricherDatasourceInterface $datasource_plugin */
          $datasource_plugin = $enricher->getDatasource($key);
          // Get datasources data based on this token.
          $datasources_output += $datasource_plugin->fetchAndProcessData($token);
        }
        // Attach all datasources output cookies to the response headers.
        if ($datasources_output) {
          foreach ($datasources_output as $datasource_output) {
            if ($datasource_output instanceof Cookie) {
              $response->headers->setCookie($datasource_output);
            }
          }
        }
      }
      return $response;
    }
    throw new NotFoundHttpException();
  }

  /**
   * Convert encoded data parameter into an PHP array.
   *
   * @param string $data
   *   A string in the format data:<base64 encoded hash>.
   *
   * @return array
   *   The incoming data converted to an array.
   */
  private function dataToArray(string $data): array {
    $keys = [];
    parse_str(base64_decode(preg_replace('/^data:/', '', $data)), $keys);
    return $keys;
  }

}
