<?php

namespace Drupal\convivial_enricher;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines the interface for enricher datasources.
 *
 * @see \Drupal\convivial_enricher\Annotation\EnricherDatasource
 * @see \Drupal\convivial_enricher\EnricherDatasourceBase
 * @see \Drupal\convivial_enricher\EnricherDatasourceManager
 * @see plugin_api
 */
interface EnricherDatasourceInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface, PluginFormInterface {

  /**
   * Returns a render array summarizing the config of enricher datasource.
   *
   * @return array
   *   A render array.
   */
  public function getSummary();

  /**
   * Returns the enricher datasource label.
   *
   * @return string
   *   The enricher datasource label.
   */
  public function label();

  /**
   * Returns the unique ID representing the enricher datasource.
   *
   * @return string
   *   The enricher datasource ID.
   */
  public function getUuid();

  /**
   * Returns the weight of the enricher datasource.
   *
   * @return int|string
   *   Either the integer weight of the enricher datasource, or an empty string.
   */
  public function getWeight();

  /**
   * Sets the weight for this enricher datasource.
   *
   * @param int $weight
   *   The weight for this enricher datasource.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * Process the incoming path for datasource.
   *
   * @param mixed $path
   *   The incoming path.
   * @param string $endpoint_path
   *   The endpoint path of an enricher entity.
   *
   * @return mixed
   *   The path returned by datasource.
   */
  public function processIncomingPath(&$path, string $endpoint_path);

  /**
   * Fetch and process the data from datasource.
   *
   * @param mixed $key
   *   The key to query the datasource with.
   *
   * @return \Symfony\Component\HttpFoundation\Cookie|null
   *   Data from the datasource.
   */
  public function fetchAndProcessData($key);

}
