<?php

namespace Drupal\convivial_enricher\Controller;

use Drupal\convivial_enricher\EnricherHelper;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for Enricher routes.
 *
 * This class is responsible for taking in the incoming data
 * from our endpoint path's data parameter and handing that over to a specific
 * endpoint path processor for processing and returning an appropriate
 * response.
 */
class EnricherController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The endpoint processor we will hand process of the data over to.
   *
   * @var \Drupal\convivial_enricher\EnricherHelper
   */
  private $enricherHelper;

  /**
   * {@inheritdoc}
   */
  public function __construct(EnricherHelper $enricherHelper) {
    $this->enricherHelper = $enricherHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('convivial_enricher.helper'),
    );
  }

  /**
   * Take the incoming data and hand it over for processing.
   *
   * @param string $enricher_id
   *   The ID of the plugin type to use to process the data parameter.
   * @param mixed $data
   *   The incoming data parameter from this request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   HTTP response, probably a redirect to the 'return_to'.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function build(string $enricher_id, $data = []): Response {
    return $this->enricherHelper->processIncomingData($data, $enricher_id);
  }

}
