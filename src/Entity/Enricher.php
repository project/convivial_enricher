<?php

namespace Drupal\convivial_enricher\Entity;

use Drupal\convivial_enricher\EnricherDatasourceInterface;
use Drupal\convivial_enricher\EnricherDatasourcePluginCollection;
use Drupal\convivial_enricher\EnricherInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Routing\RouteBuilderInterface;

/**
 * Defines an enricher configuration entity.
 *
 * @ConfigEntityType(
 *   id = "enricher",
 *   label = @Translation("Enricher"),
 *   label_collection = @Translation("Enrichers"),
 *   label_singular = @Translation("enricher"),
 *   label_plural = @Translation("enrichers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count enricher",
 *     plural = "@count enrichers",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\convivial_enricher\EnricherListBuilder",
 *     "form" = {
 *       "add" = "Drupal\convivial_enricher\Form\EnricherAddForm",
 *       "edit" = "Drupal\convivial_enricher\Form\EnricherEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "enricher",
 *   admin_permission = "administer enricher",
 *   links = {
 *     "collection" = "/admin/config/convivial/enricher",
 *     "edit-form" = "/admin/config/convivial/enricher/manage/{enricher}",
 *     "delete-form" = "/admin/config/convivial/enricher/manage/{enricher}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "name",
 *     "label",
 *     "status",
 *     "description",
 *     "endpoint_path",
 *     "datasources",
 *   }
 * )
 */
class Enricher extends ConfigEntityBase implements EnricherInterface, EntityWithPluginCollectionInterface {

  /**
   * The name of the enricher.
   *
   * @var string
   */
  protected $name;

  /**
   * The enricher label.
   *
   * @var string
   */
  protected $label;

  /**
   * The enricher status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The enricher description.
   *
   * @var string
   */
  protected $description;

  /**
   * The path used to create a route and accept incoming data.
   *
   * The endpoint_path is passed to our route subscriber and a route with that
   * path is created.
   * The system uses this path to read data and process it, for example to
   * read a user id and query an api with it.
   *
   * @var string
   */
  protected $endpoint_path;

  /**
   * The array of enricher datasources for this enricher.
   *
   * @var array
   */
  protected $datasources = [];

  /**
   * Collection of enricher datasources that are used by this enricher.
   *
   * @var \Drupal\convivial_enricher\EnricherDatasourcePluginCollection
   */
  protected $datasourcesCollection;

  /**
   * Drupal routeBuilder object, for rebuilding routes on form save.
   *
   * @var \Drupal\Core\Routing\RouteBuilder
   */
  private $routeBuilder;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name');
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    $this->getRouteBuilder()->rebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function getDatasource($datasource) {
    return $this->getDatasources()->get($datasource);
  }

  /**
   * {@inheritdoc}
   */
  public function getDatasources() {
    if (!$this->datasourcesCollection) {
      $this->datasourcesCollection = new EnricherDatasourcePluginCollection($this->getEnricherDatasourcePluginManager(), $this->datasources);
      $this->datasourcesCollection->sort();
    }
    return $this->datasourcesCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'datasources' => $this->getDatasources(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteEnricherDatasource(EnricherDatasourceInterface $datasource) {
    $this->getDatasources()->removeInstanceId($datasource->getUuid());
    $this->save();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addEnricherDatasource(array $configuration) {
    $configuration['uuid'] = $this->uuidGenerator()->generate();
    $this->getDatasources()->addInstanceId($configuration['uuid'], $configuration);
    return $configuration['uuid'];
  }

  /**
   * Retrieve the drupal route builder service.
   *
   * There is currently no way to inject services into config entities
   * so that this little pseudo-factory can do it for us.
   *
   * See: https://www.drupal.org/project/drupal/issues/2142515
   *
   * @return \Drupal\Core\Routing\RouteBuilderInterface
   *   A Drupal route builder.
   */
  private function getRouteBuilder(): RouteBuilderInterface {
    if (!isset($this->routeBuilder)) {
      $this->routeBuilder = \Drupal::service('router.builder');
    }
    return $this->routeBuilder;
  }

  /**
   * Returns the enricher data source plugin manager.
   *
   * @return \Drupal\Component\Plugin\PluginManagerInterface
   *   The enricher datasource plugin manager.
   */
  protected function getEnricherDatasourcePluginManager() {
    return \Drupal::service('plugin.manager.enricher.datasource');
  }

}
