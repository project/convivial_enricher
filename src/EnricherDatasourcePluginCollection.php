<?php

namespace Drupal\convivial_enricher;

use Drupal\Core\Plugin\DefaultLazyPluginCollection;

/**
 * A collection of enricher datasources.
 */
class EnricherDatasourcePluginCollection extends DefaultLazyPluginCollection {

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\convivial_enricher\EnricherDatasourceInterface
   *   The convivial enricher datasource object.
   */
  public function &get($instance_id) {
    return parent::get($instance_id);
  }

  /**
   * {@inheritdoc}
   */
  public function sortHelper($aID, $bID) {
    return $this->get($aID)->getWeight() <=> $this->get($bID)->getWeight();
  }

}
