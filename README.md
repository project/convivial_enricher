# Convivial Enricher

## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

## INTRODUCTION

The Enricher module provides automatic progressive profiling by enabling the
enrichment of the user's profile from varied backend datasources.

Good marketing hinges on communicating the usefulness of your product. That
means knowing who your customers are and how they'll use your product.

With profile enrichment from the enricher module you can increase your
visibility of your audience and differentiate your customers.
your customers to differentiate them. By knowing who your customers are, you are
empowered to target your marketing approaches by adapting to how they want
to use your product.

Let's take an active campaign email campaign as an example.
You send out an email to your mailing list which includes a call-to-action link
and a unique identifier for the active campaign user and a landing page as slugs
or parameters to of the URL.

When the customer clicks the link in the email, the enricher module, along with
the active campaign datasource plugin reads the unique identifier and reaches
back into active campaign to retrieve more user data and stores this in a
temporary cookie for retrieval by profile tools such as the basil module.

## REQUIREMENTS

* [Convivial Core](https://www.drupal.org/project/convivial_core)

## SUB MODULES

 * Convivial Enricher ActiveCampaign
 * Convivial Enricher Mailchimp
 * Convivial Enricher Recombee

## INSTALLATION

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

 * The Enricher ActiveCampaign submodule is packages with the enricher module.
   You can enabled this to allow configuration of enrichment from active
   campaign.

 * There is a dummy datasource plugin available for your testing purposes. To
   query real data you will need to enable and configure the Enricher Active
   campaign module or you can write a new plugin of your own.

## CONFIGURATION

 * Configure the user permissions in Administration » People » Permissions:

   - Administer enrichers

     Users with this permission will see the Web services > enrichers
     configuration list page. From here they can add, configure, delete, enable
     and disabled enrichers.

     Warning: Give to trusted roles only; this permission has security
     implications. Allows full administration access to create and edit
     enrichers.

## TROUBLESHOOTING

 * If you are not receiving data back for your user.

   - Check the recent log messages report for exception messages.

 * If you are stuck in a redirect loop.

   - The issue appears in using this module together with the redirect module.
   See the redirect module issue
  https://www.drupal.org/project/redirect/issues/3037259

## MAINTAINERS

This module is maintained by developers at Morpht. For more information on the
company and our offerings, see [morpht.com](https://morpht.com/).
