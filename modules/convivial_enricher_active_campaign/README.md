# Convivial Enricher ActiveCampaign

## CONTENTS OF THIS FILE

 * Introduction
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

## INTRODUCTION

The Enricher Active campaign is a submodule of Enricher. Enricher ActiveCampaign provides an implementation of an EnricherDatasource plugin to allow Enricher to interface with ActiveCampaign's REST API.

## INSTALLATION

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

## CONFIGURATION

 * Configure the user permissions in Administration » People » Permissions:

   - Administer enrichers

     Users with this permission will see the Web services > enrichers
     configuration list page. From here they can add, configure, delete, enable
     and disabled enrichers.

     Warning: Give to trusted roles only; this permission has security
     implications. Allows full administration access to create and edit
     enrichers.

## TROUBLESHOOTING

 * If you are not receiving data back for your user.

   - Check the recent log messages report for exception messages.

## MAINTAINERS

This module is maintained by developers at Morpht. For more information on the
company and our offerings, see [morpht.com](https://morpht.com/).
