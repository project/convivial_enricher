<?php

namespace Drupal\convivial_enricher_active_campaign;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ActiveCampaignPhpApiAdaptor.
 *
 * Provides an adapter for active campaign APIs.
 *
 * This class is responsible for communicating with ActiveCampaign's REST API.
 *
 * It makes use of the Guzzle HTTP client to communicate with the REST
 * endpoints directly.
 */
class ActiveCampaignPhpApiAdaptor implements ActiveCampaignPhpApiAdaptorInterface {

  /**
   * The HTTP client to communicate with REST.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Drupal logger channel factory service.
   *
   * For logging exceptions.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * The API key of the active campaign account we are accessing.
   *
   * @var string
   * @see \Drupal\convivial_enricher_active_campaign\ActiveCampaignPhpApiAdaptorInterface::setApiKey()
   */
  private $apiKey;

  /**
   * The BaseURL of the ActiveCampaign REST API.
   *
   * @var string
   * @see \Drupal\convivial_enricher_active_campaign\ActiveCampaignPhpApiAdaptorInterface::setBaseUrl()
   */
  private $baseUrl;

  /**
   * The part of the API URL following api/3 not including the query string.
   *
   * @var string
   */
  private $endpoint;

  /**
   * Array of parameters that will become the query string in the REST call.
   *
   * @var array
   */
  private $queryParameters;

  /**
   * The cache details used for the next API call.
   *
   * These are cleared by makeApiCall() on completion.
   *
   * @var array
   *
   * @see \Drupal\convivial_enricher_active_campaign\ActiveCampaignPhpApiAdaptorInterface::makeApiCall()
   */
  private $cacheDetails;

  /**
   * Cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cacheBackend;

  /**
   * Construct a new ActiveCampaignPhpApiAdaptor.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   Client for communicating with active campaigns REST api.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Drupal logger service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend service.
   */
  public function __construct(ClientInterface $httpClient, LoggerChannelFactoryInterface $loggerChannelFactory, CacheBackendInterface $cacheBackend) {
    $this->httpClient           = $httpClient;
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->cacheBackend         = $cacheBackend;
  }

  /**
   * {@inheritdoc}
   */
  public function setApiKey(string $api_key) {
    $this->apiKey = $api_key;
  }

  /**
   * {@inheritdoc}
   */
  public function setBaseUrl(string $base_url) {
    $this->baseUrl = $base_url;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \InvalidArgumentException
   */
  public function setCacheDetailsForNextApiCall(array $cache_details): void {
    $default_cache_details = ['expiry' => CacheBackendInterface::CACHE_PERMANENT];
    if (empty($cache_details['cid'])) {
      throw new \InvalidArgumentException("No cid specified in \$cache_details parameter.");
    }
    $this->cacheDetails = $cache_details + $default_cache_details;
  }

  /**
   * {@inheritdoc}
   */
  public function makeApiCall($endpoint, array $query_parameters = []) {
    $this->endpoint = $endpoint;
    $this->queryParameters = $query_parameters;

    if ($data = $this->getCachedResponseData()) {
      $this->cacheDetails = [];
      return $data;
    }

    try {
      $response = $this->getResponseFromApi();
    }
    catch (RequestException | GuzzleException | \InvalidArgumentException $exception) {
      $variables = Error::decodeException($exception);
      $this->loggerChannelFactory->get('convivial_enricher_active_campaign')->warning('%type: @message in %function (line %line of %file).', $variables);
      return FALSE;
    }

    $response_content = $response->getBody()->getContents();
    if (!empty($this->cacheDetails['cid'])) {
      $cid = $this->generateCacheBinId();
      $this->cacheBackend->set($cid, $response_content, $this->cacheDetails['expiry']);
    }
    $this->cacheDetails = [];
    return $response_content;
  }

  /**
   * Create an Active campaign request URL usable by Guzzle.
   *
   * @return string
   *   An Active campaign request URL usable by Guzzle.
   *
   * @throws \InvalidArgumentException
   */
  private function generateRequestUrlFromEndpointPathAndParameters() {
    if (empty($this->baseUrl)) {
      throw new \InvalidArgumentException("No Base URL has been configured for active campaign API.");
    }
    return Url::fromUri($this->baseUrl . '/api/3/' . $this->endpoint, ['query' => $this->queryParameters])
      ->toString();
  }

  /**
   * Generate a guzzle options array including our API key.
   *
   * @see https://docs.guzzlephp.org/en/stable/request-options.html#headers
   * @see https://developers.activecampaign.com/reference#authentication
   *
   * @return array
   *   Headers array for use in Guzzle options.
   *
   * @throws \InvalidArgumentException
   */
  private function generateRequestOptionsWithHeaders(): array {
    if (empty($this->apiKey)) {
      throw new \InvalidArgumentException("No API key has been configured for active campaign API.");
    }
    return ['headers' => ['Api-Token' => $this->apiKey]];
  }

  /**
   * Retrieve available cached data for the API call.
   *
   * @return string|bool
   *   The cache response or false if none available.
   */
  private function getCachedResponseData(): string {
    if (!empty($this->cacheDetails['cid'])) {
      $cid = $this->generateCacheBinId();
      if ($cache = $this->cacheBackend->get($cid)) {
        return $cache->data;
      }
    }
    return FALSE;
  }

  /**
   * Gather the request URL, options and perform the request.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   This request's response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \InvalidArgumentException
   */
  private function getResponseFromApi(): ResponseInterface {
    $request_url = $this->generateRequestUrlFromEndpointPathAndParameters();
    $options     = $this->generateRequestOptionsWithHeaders();
    return $this->httpClient->request('GET', $request_url, $options);
  }

  /**
   * Generate a unique cache bin for this account and API call type.
   *
   * @return string
   *   MD5 encoded representation of the identifying data for this cache bin.
   */
  private function generateCacheBinId(): string {
    return md5($this->apiKey . $this->baseUrl . 'convivial_enricher_active_campaign_api-' . $this->cacheDetails['cid']);
  }

}
