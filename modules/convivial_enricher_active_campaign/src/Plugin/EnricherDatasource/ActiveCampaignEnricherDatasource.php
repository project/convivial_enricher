<?php

namespace Drupal\convivial_enricher_active_campaign\Plugin\EnricherDatasource;

use Drupal\convivial_enricher\EnricherDatasourceBase;
use Drupal\convivial_enricher_active_campaign\ActiveCampaignPhpApiAdaptorInterface;
use Drupal\convivial_enricher_active_campaign\Exception\ContactHashMismatchException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the datasource.
 *
 * @EnricherDatasource(
 *   id = "active_campaign",
 *   label = @Translation("Active Campaign"),
 *   description = @Translation("Active campaign datasource for enricher."),
 * )
 */
class ActiveCampaignEnricherDatasource extends EnricherDatasourceBase implements ContainerFactoryPluginInterface {

  /**
   * ActiveCampaign API client.
   *
   * @var array|\Drupal\convivial_enricher_active_campaign\ActiveCampaignPhpApiAdaptorInterface
   */
  private $activeCampaignPhpApiAdaptor;

  /**
   * An object representing a contact of active campaign.
   *
   * @var object
   */
  private $contact;

  /**
   * The endpoint Path.
   *
   * @var string
   */
  protected $endpointPath;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, ActiveCampaignPhpApiAdaptorInterface $activeCampaignPhpApiAdaptor) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger);
    $this->activeCampaignPhpApiAdaptor = $activeCampaignPhpApiAdaptor;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    $container->get('logger.factory')->get('convivial_enricher'),
      $container->get('convivial_enricher_active_campaign.api'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = parent::getSummary();
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'active_campaign_api_key' => NULL,
      'active_campaign_base_url' => NULL,
      'allow_list' => [],
      'privacy' => [],
      'cache_settings' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $help_link = Link::fromTextAndUrl('Active campaign authentication documentation', Url::fromUri('https://developers.activecampaign.com/reference#authentication'));
    $help = nl2br($this->t("Your API key can be found in active campaign on the Settings page under the 'Developer' tab.
Each user in your ActiveCampaign account has their own unique API key.

See @help_link", ['@help_link' => $help_link->toString()]));

    $form['active_campaign_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#maxlength' => 255,
      '#default_value' => $this->configuration['active_campaign_api_key'] ?? '',
      '#description' => $help,
      '#required' => TRUE,
      '#attributes' => ['class' => ['datasource-plugin-field-api-key']] ?? '',
    ];

    $help_link = Link::fromTextAndUrl('Active campaign Base URL documentation', Url::fromUri('https://developers.activecampaign.com/reference#url'));
    $help = nl2br($this->t("The API is accessed using a base URL that is specific to your active campaign account.
Your API URL can be found in your active campaign account on the My Settings page under the \"Developer\" tab.

See @help_link", ['@help_link' => $help_link->toString()]));

    $form['active_campaign_base_url'] = [
      '#type' => 'url',
      '#title' => $this->t('API Base Url'),
      '#description' => $help,
      '#required' => FALSE,
      '#default_value' => $this->configuration['active_campaign_base_url'] ?? '',
      '#attributes' => ['class' => ['datasource-plugin-field-base-url']],
    ];

    $form['allow_list'] = [
      '#type' => 'details',
      '#title' => $this->t('Allow lists'),
      '#description' => $this->t("Provide lists of contact tags and properties that are allowed to be stored by this enricher."),
    ];

    $help = nl2br($this->t("Enter a list of allowed parameters to be retrieved, one per line. All other parameters are ignored.
The '*' character is a wildcard. All entries are treated as a shell wildcard pattern.
Examples:
  123* matches every tag that starts with 123.
  *123 matches every tag that ends 123.
  *123* matches every tag that contains 123.

Other shell wildcards such as ., ?, !, [] are honored for advanced users."));

    $form['allow_list']['contact_properties'] = [
      '#title' => $this->t('Contact Properties'),
      '#type' => 'textarea',
      '#default_value' => $this->configuration['allow_list']['contact_properties'] ?? '',
      '#description' => $help,
    ];

    $help = nl2br($this->t("Enter a list of allowed tags to be retrieved, one per line. All other tags are ignored.
The '*' character is a wildcard. All entries are treated as a shell wildcard pattern.
Examples:
  123* matches every tag that starts with 123.
  *123 matches every tag that ends 123.
  *123* matches every tag that contains 123.

Other shell wildcards such as ., ?, !, [] are honored for advanced users."));

    $form['allow_list']['contact_tags'] = [
      '#title' => $this->t('Contact Tags'),
      '#type' => 'textarea',
      '#default_value' => $this->configuration['allow_list']['contact_tags'] ?? '',
      '#description' => $help,
    ];

    $help = nl2br($this->t("Enter a list of allowed fields to be retrieved, one per line. All other parameters are ignored.
The '*' character is a wildcard. All entries are treated as a shell wildcard pattern.
Examples:
  123* matches every tag that starts with 123.
  *123 matches every tag that ends 123.
  *123* matches every tag that contains 123.

Other shell wildcards such as ., ?, !, [] are honored for advanced users."));

    $form['allow_list']['contact_fields'] = [
      '#title' => $this->t('Contact Fields'),
      '#type' => 'textarea',
      '#default_value' => $this->configuration['allow_list']['contact_fields'] ?? '',
      '#description' => $help,
    ];

    $form['privacy'] = [
      '#type' => 'details',
      '#title' => $this->t('Privacy'),
      '#description' => $this->t("Control how a contact's cookie privacy is handled."),
    ];

    $form['privacy']['enabled'] = [
      '#title' => $this->t("Enable honoring of a contact's property as a privacy flag"),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['privacy']['enabled'] ?? TRUE,
      '#description' => $this->t("Check this box to allow configuration of an active campaign contact's property to check for privacy handling."),
      '#attributes' => ['class' => ['privacy']],
    ];

    $form['privacy']['property_name'] = [
      '#title' => $this->t('Privacy property name'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['privacy']['property_name'] ?? 'privacy_accepted',
      '#description' => $this->t("This is an Opt-in setting. It must be set TRUE for the user to be enriched."),
      '#states' => [
        'disabled' => [
          '.privacy' => [
            'checked' => FALSE,
          ],
        ],
        'required' => [
          '.privacy' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $help_link = Link::fromTextAndUrl('Active campaign rate limit documentation', Url::fromUri('https://developers.activecampaign.com/reference#rate-limits'));

    $why_cache_help_text = nl2br($this->t("In order to compile a complete list of tags for a contact, three API calls
are required to be made to Active Campaign.

Active campaign applies rate limiting on these calls.
\"Our API has a rate limit of 5 requests per second per account.\"
- @link
We can cache the results to reduce the number of calls required.

The first call retrieves a list of all tags available to us on active campaign.
The second call retrieves the details of the contact include the contact's ID.
The third call retrieves the IDs of the tags on that contact.

Finally we do a match on the contact's tag IDs against the available tags
on this account so as to compile the contact's tag names.

It's unnecessary to look up all these details every time, especially
the available tags on this account. We can use these settings to disable
and control the caching of each of these calls independently.", ['@link' => $help_link->toString()]));

    $form['cache_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('API Cache'),
      '#description' => $why_cache_help_text,
    ];

    $expiry_format_help_text = nl2br($this->t("You may enter just about any English textual datetime description.

Examples:
now
10 September 2000
+1 day
+1 week
+1 week 2 days 4 hours 2 seconds
next Thursday
last Monday

See: https://www.php.net/manual/en/function.strtotime.php for more details.

Leave empty to cache permanently."));

    $form['cache_settings']['account_tags'] = [
      '#title' => $this->t('Account tags'),
      '#type' => 'fieldset',
    ];

    $form['cache_settings']['account_tags']['enabled'] = [
      '#title' => $this->t('Cache account tags'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['cache_settings']['account_tags']['enabled'] ?? TRUE,
      '#description' => $this->t("Enabled caching on retrieval of tags available on this account."),
      '#attributes' => ['class' => ['account_tags_enabled']],
    ];
    $form['cache_settings']['account_tags']['expiry'] = [
      '#title' => $this->t('Cache expiry'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['cache_settings']['account_tags']['expiry'] ?? "+1 day",
      '#description' => $expiry_format_help_text,
      '#states' => [
        'visible' => [
          '.account_tags_enabled' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];
    $form['cache_settings']['contact_tags'] = [
      '#title' => $this->t('Contact tags'),
      '#type' => 'fieldset',
    ];
    $form['cache_settings']['contact_tags']['enabled'] = [
      '#title' => $this->t('Cache contact tags'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['cache_settings']['contact_tags']['enabled'] ?? TRUE,
      '#description' => $this->t("Enabled caching on retrieval of tags of this contact."),
      '#attributes' => ['class' => ['contact_tags_enabled']],
    ];
    $form['cache_settings']['contact_tags']['expiry'] = [
      '#title' => $this->t('Cache expiry'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['cache_settings']['contact_tags']['expiry'] ?? '+1 hour',
      '#description' => $expiry_format_help_text,
      '#states' => [
        'visible' => [
          '.contact_tags_enabled' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];
    $form['cache_settings']['contact'] = [
      '#title' => $this->t('Contact'),
      '#type' => 'fieldset',
    ];
    $form['cache_settings']['contact']['enabled'] = [
      '#title' => $this->t('Cache contact'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['cache_settings']['contact']['enabled'] ?? TRUE,
      '#description' => $this->t("Enabled caching on retrieval of contact."),
      '#attributes' => ['class' => ['contact_enabled']],
    ];
    $form['cache_settings']['contact']['expiry'] = [
      '#title' => $this->t('Cache expiry'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['cache_settings']['contact']['expiry'] ?? '+1 hour',
      '#description' => $expiry_format_help_text,
      '#states' => [
        'visible' => [
          '.contact_enabled' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['active_campaign_api_key'] = $form_state->getValue('active_campaign_api_key');
    $this->configuration['active_campaign_base_url'] = $form_state->getValue('active_campaign_base_url');
    $this->configuration['allow_list']['contact_properties'] = $form_state->getValue('allow_list')['contact_properties'];
    $this->configuration['allow_list']['contact_tags'] = $form_state->getValue('allow_list')['contact_tags'];
    $this->configuration['allow_list']['contact_fields'] = $form_state->getValue('allow_list')['contact_fields'];
    $this->configuration['privacy']['enabled'] = $form_state->getValue('privacy')['enabled'];
    $this->configuration['privacy']['property_name'] = $form_state->getValue('privacy')['property_name'];
    $this->configuration['cache_settings']['account_tags']['enabled'] = $form_state->getValue('cache_settings')['account_tags']['enabled'];
    $this->configuration['cache_settings']['account_tags']['expiry'] = $form_state->getValue('cache_settings')['account_tags']['expiry'];
    $this->configuration['cache_settings']['contact_tags']['enabled'] = $form_state->getValue('cache_settings')['contact_tags']['enabled'];
    $this->configuration['cache_settings']['contact_tags']['expiry'] = $form_state->getValue('cache_settings')['contact_tags']['expiry'];
    $this->configuration['cache_settings']['contact']['enabled'] = $form_state->getValue('cache_settings')['contact']['enabled'];
    $this->configuration['cache_settings']['contact']['expiry'] = $form_state->getValue('cache_settings')['contact']['expiry'];
  }

  /**
   * {@inheritdoc}
   *
   * Take the incoming path and process it if necessary.
   *
   * Sample use case: converting /mypath/<token>/return/to/path, as
   * drupal does not allow / is parameters, we would need to process this url
   * to encode it differently and then drupal can digest the new path.
   *
   * This is designed to be called from our
   * \Drupal\convivial_enricher\PathProcessor\InboundPathProcessor
   */
  public function processIncomingPath(&$path, $endpointPath) {
    $this->endpointPath = $endpointPath;
    return $this->convertPathToDataEncodedString($path);
  }

  /**
   * Modify the incoming path/convert incoming path to something usable.
   *
   * @param string $path
   *   The incoming path string. eg: The /my/path portion of the incoming URL.
   *
   * @return bool
   *   TRUE if path was converted by this method, false if not.
   */
  private function convertPathToDataEncodedString(string &$path): string {
    if ($this->pathRequiresConversion($path)) {
      $token     = $this->getTokenFromPath($path);
      $return_to = $this->getReturnToPortionOfPath($path);

      $encoded_data_string = base64_encode("return_to={$return_to}&token={$token}");

      $path = '/' . $this->endpointPath . "/data:$encoded_data_string";
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Determine if the format of this incoming path requires conversion.
   *
   * @param string $path
   *   The incoming path string. eg: The /my/path portion of the incoming URL.
   *
   * @return bool
   *   TRUE if path requires conversion to a drupal usable format.
   */
  private function pathRequiresConversion(string $path): bool {
    return $this->pathIsAnEndpointPath($path, $this->endpointPath) && $this->pathDoesNotHaveLoadedData($path, $this->endpointPath);
  }

  /**
   * Get the token from the provided path string.
   *
   * @param string $path
   *   The incoming path string. eg: The /my/path portion of the incoming URL.
   *
   * @return string
   *   The token value calculated from the supplied path string.
   */
  private function getTokenFromPath(string $path) {
    $token = preg_replace('|^\/' . $this->endpointPath . '\/(.*?)\/.*$|', '$1', $path);
    return $token;
  }

  /**
   * Get the return_to parameter encoded in the path.
   *
   * @param string $path
   *   The incoming path string. eg: The /my/path portion of the incoming URL.
   *
   * @return string
   *   The return_to value calculated from the supplied path string.
   */
  private function getReturnToPortionOfPath(string $path): string {
    $return_to = preg_replace('|^\/' . $this->endpointPath . '\/.*?(\/.*)$|', '$1', $path);
    return $return_to;
  }

  /**
   * Determine if the given path is our endpoint.
   *
   * Compare the path to the endpointPath to determine if this is one of
   * our defined endpoints.
   *
   * @param string $path
   *   The incoming path string. eg: The /my/path portion of the incoming URL.
   *
   * @return bool
   *   TRUE if path is a processable endpoint.
   */
  private function pathIsAnEndpointPath(string $path): bool {
    return strpos($path, "/$this->endpointPath/") === 0;
  }

  /**
   * Analyze this path to determine if it contains encoded data we can read.
   *
   * @param string $path
   *   The incoming path string. eg: The /my/path portion of the incoming URL.
   *
   * @return bool
   *   TRUE if path contains encoded data, FALSE if not.
   */
  private function pathDoesNotHaveLoadedData(string $path): bool {
    return strpos($path, "/$this->endpointPath/data:") !== 0;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchAndProcessData($key) {
    $this->initializeActiveCampaignApiCredentials();
    $output = [];
    try {
      $this->contact = $this->getContactFromEmailHash($key);
    }
    catch (\Exception $exception) {
      $variables = Error::decodeException($exception);
      $this->logger->warning('%type: @message in %function (line %line of %file).', $variables);
    }

    if (!empty((array) $this->contact)) {
      $contact = $this->filterContactPropertiesByAcceptList();
      $output += $this->getContactTagsFilteredByAcceptList();
      $output += $this->getContactFieldsFilteredByAcceptList();

      if ($this->isPrivacyEnabled()) {
        if ($this->hasUserAllowedTracking()) {
          $output += $contact;
        }
      }
      else {
        $output += $contact;
      }
    }
    $return_cookies = [];
    // Create the cookies of all the output fields.
    foreach ($output as $cookie_name => $cookie_value) {
      $return_cookies[] = $this->createCookie($cookie_name, $cookie_value);
    }
    return $return_cookies;
  }

  /**
   * Get an array of tags for this datasource instance's contact.
   *
   * @return array
   *   Array of tag names keyed by tag id.
   */
  private function getContactTags(): array {
    $contact_tags_ids = $this->getTagIdsOfContact();
    $account_tags = $this->getTagsOfAccount();
    return $this->getTagAndNameFromContactsTagIdsAndAccountTags($contact_tags_ids, $account_tags);
  }

  /**
   * Look up a contact in active campaign by email_hash.
   *
   * @param string $email_hash
   *   This contact's email_hash property value in active campaign.
   *
   * @return object
   *   The contact object retrieved from active campaign.
   *
   * @throws \Drupal\convivial_enricher_active_campaign\Exception\ContactHashMismatchException
   * @throws \InvalidArgumentException
   */
  private function getContactFromEmailHash(string $email_hash): \stdClass {

    if (!empty($this->configuration["cache_settings"]["contact"]["enabled"])) {
      $this->activeCampaignPhpApiAdaptor->setCacheDetailsForNextApiCall([
        'cid'    => 'contact:' . $email_hash,
        'expiry' => strtotime($this->configuration["cache_settings"]["contact"]["expiry"]),
      ]);
    }

    if ($response = $this->activeCampaignPhpApiAdaptor->makeApiCall('contacts', ['email_hash' => $email_hash])) {
      $response = json_decode($response);
      $this->checkCachedUserIdEmailHashForChanges($response, $email_hash);
      return $response->contacts[0];
    }

    return (object) [];
  }

  /**
   * Get an array of tag IDs for this contact from active campaign.
   *
   * @return array
   *   An array of tag IDs for this contact from active campaign.
   */
  private function getTagIdsOfContact(): array {

    if (!empty($this->configuration["cache_settings"]["contact_tags"]["enabled"])) {
      $this->activeCampaignPhpApiAdaptor->setCacheDetailsForNextApiCall([
        'cid'    => 'contact_tags_ids:' . $this->contact->id,
        'expiry' => strtotime($this->configuration["cache_settings"]["contact_tags"]["expiry"]),
      ]);
    }

    $response = $this->activeCampaignPhpApiAdaptor->makeApiCall('contacts/' . $this->contact->id . '/contactTags');
    $contact_tags = [];
    if ($response) {
      $response = json_decode($response);

      $contact_tags_response = $response->contactTags;

      foreach ($contact_tags_response as $contact_tag_response) {
        $contact_tags[] = $contact_tag_response->tag;
      }
    }

    return $contact_tags;
  }

  /**
   * Get an array of field values for this contact from active campaign.
   *
   * @return array
   *   An array of field value for this contact from active campaign.
   */
  private function getAccountFields(): array {
    $response = $this->activeCampaignPhpApiAdaptor->makeApiCall('contacts/' . $this->contact->id . '/fieldValues');
    $field_values = [];
    if ($response) {
      $response = json_decode($response);
      $field_values_response = $response->fieldValues;
      foreach ($field_values_response as $field_value_response) {
        $field_response = $this->activeCampaignPhpApiAdaptor->makeApiCall('fieldValues/' . $field_value_response->id . '/field');
        if ($field_response) {
          $field_response = json_decode($field_response);
          $field_values[$field_response->field->title] = $field_value_response->value;
        }
      }
    }
    return $field_values;
  }

  /**
   * Get an array of this account's tag names keyed by tag id.
   *
   * @return array
   *   An array of this account's tag names keyed by tag id.
   */
  private function getTagsOfAccount(): array {
    if (!empty($this->configuration["cache_settings"]["account_tags"]["enabled"])) {
      $this->activeCampaignPhpApiAdaptor->setCacheDetailsForNextApiCall([
        'cid'    => 'account_tags',
        'expiry' => strtotime($this->configuration["cache_settings"]["account_tags"]["expiry"]),
      ]);
    }

    $response = $this->activeCampaignPhpApiAdaptor->makeApiCall('tags');

    $tags = [];
    if ($response) {
      $response = json_decode($response);

      foreach ($response->tags as $response_tag) {
        $tags[$response_tag->id] = $response_tag->tag;
      }
    }

    return $tags;
  }

  /**
   * Get contact's tag names by looking up the IDs in the account tags.
   *
   * Take the contacts tag IDs and the account tag IDs/name array and match
   * tag names for the contact.
   *
   * @param array $contact_tags_ids
   *   The contact's tags IDs from active campaign.
   * @param array $account_tags
   *   The account's tag names and IDs from active campaign.
   *
   * @return array
   *   Array of tag names keyed by tag id.
   */
  private function getTagAndNameFromContactsTagIdsAndAccountTags(array $contact_tags_ids, array $account_tags): array {
    $tags = [];
    foreach ($contact_tags_ids as $contact_tag) {
      if (array_key_exists($contact_tag, $account_tags)) {
        $tags[$contact_tag] = $account_tags[$contact_tag];
      }
    }
    return $tags;
  }

  /**
   * Initialize the API with authentication data.
   */
  private function initializeActiveCampaignApiCredentials(): void {
    $this->activeCampaignPhpApiAdaptor->setApiKey($this->configuration['active_campaign_api_key'] ?? '');
    $this->activeCampaignPhpApiAdaptor->setBaseUrl($this->configuration['active_campaign_base_url'] ?? '');
  }

  /**
   * Return the $key_values filtered on key by the patterns in the allow_list.
   *
   * @param array $key_values
   *   An array of keys and values to filter.
   * @param array $allow_list
   *   A list of patterns suitable for fnmatch() used to filter by key.
   *
   * @return array
   *   A filtered array of keys and values.
   *
   *   With keys matching patterns in allow_list removed.
   */
  private function filterKeyValueSetOnAcceptList(array $key_values, array $allow_list): array {

    foreach ($key_values as $key => $value) {

      $tag_allowed = FALSE;
      foreach ($allow_list as $pattern) {
        // Ignore PHPCS Warning for "The use of function fnmatch() is
        // discouraged"
        // According to PHP's documentation
        // "For now, this function is not available on non-POSIX compliant
        // systems except Windows."
        // - https://www.php.net/manual/en/function.fnmatch.php
        // Running this on a non-posix compliant system and not windows
        // is very unlikely.
        // @codingStandardsIgnoreStart
        if (is_string($value) && is_string($key) && fnmatch($pattern, $key)) {
          // @codingStandardsIgnoreEnd
          $tag_allowed = TRUE;
        }
      }

      if (!$tag_allowed) {
        unset($key_values[$key]);
      }

    }

    return $key_values ?? [];
  }

  /**
   * Remove any not string top level properties from contact object.
   *
   * @param object $contact
   *   A contact details form active campaign as an stdClass.
   *
   * @return array
   *   An array of the contact's properties keyed by name.
   *   Example:
   *   [
   *     'firstName => 'Jane',
   *     'lastName => 'Doe',
   *   ];
   */
  private function prepareContactForFiltering(object $contact): array {
    $contact = (array) $contact;
    foreach ($contact as $key => $value) {
      if (!is_string($value) && !is_numeric($value)) {
        unset($contact[$key]);
      }
    }
    return array_flip($contact) ?? [];
  }

  /**
   * Get contact properties filtered by the enricher's properties' allow list.
   *
   * @return array
   *   An array of properties of this contact keyed by property name.
   *
   *   Only includes properties that match to the enricher's allow list.
   *
   *   Example:
   *   [
   *     'firstName => 'Jane',
   *     'lastName => 'Doe',
   *   ];
   */
  private function filterContactPropertiesByAcceptList(): array {
    $accepted_properties = explode(PHP_EOL, $this->configuration['allow_list']['contact_properties']);
    $contact             = $this->prepareContactForFiltering($this->contact);
    $filtered_contact    = $this->filterKeyValueSetOnAcceptList($contact, $accepted_properties);
    return array_flip($filtered_contact);
  }

  /**
   * Get the contacts tags, only allowed tags.
   *
   * Get the tags associated with the contact of this datasource instance,
   * ensuring disallowed tags are filtered out.
   *
   * @return array
   *   An array of tags of this contact keyed by tag id.
   *
   *   Only includes tags that match to the Enricher's allow list.
   *
   *   Example:
   *   [
   *     7 => 'web.morpht',
   *   ];
   */
  private function getContactTagsFilteredByAcceptList(): array {
    $tags = $this->getContactTags();
    $accepted_tags = explode(PHP_EOL, $this->configuration['allow_list']['contact_tags']);
    return $this->filterKeyValueSetOnAcceptList($tags, $accepted_tags);
  }

  /**
   * Get the contacts fields, only allowed fields.
   *
   * Get the fields associated with the contact of this datasource instance,
   * ensuring disallowed tags are filtered out.
   *
   * @return array
   *   An array of fields of this contact keyed by tag id.
   *
   *   Only includes tags that match to the Enricher's allow list.
   *
   *   Example:
   *   [
   *     7 => 'web.morpht',
   *   ];
   */
  private function getContactFieldsFilteredByAcceptList(): array {
    $fields = $this->getAccountFields();
    $accepted_fields = explode(PHP_EOL, $this->configuration['allow_list']['contact_fields']);
    return $this->filterKeyValueSetOnAcceptList($fields, $accepted_fields);
  }

  /**
   * Cache the contact id with their hash.
   *
   * We can match the incoming email_hash against the contact id and if they
   * have changed we can prevent an email hash spoofing attempt.
   *
   * This only protects in the scenario where someone has managed to change
   * their own email address in active campaign. This could be achieved if
   * an automated newsletter sign up form for an authenticated user was used
   * and the newsletter sent to the list without first requiring an email
   * confirmation from the registered user.
   *
   * @param object $response
   *   A json_decoded response from the active campaign API.
   * @param string $email_hash
   *   The contact's email hash as retrieved from active_campaign.
   *
   * @throws \Drupal\convivial_enricher_active_campaign\Exception\ContactHashMismatchException
   */
  private function checkCachedUserIdEmailHashForChanges(object $response, string $email_hash): void {
    $contact_id = $response->contacts[0]->id;
    $cid        = md5($this->configuration['active_campaign_api_key'] . $this->configuration['active_campaign_base_url'] . 'active_campaign_datasource-contact-' . $contact_id);
    if ($cache = \Drupal::cache()->get($cid)) {
      $cached_email_hash = $cache->data;
      if ($cached_email_hash != $email_hash) {
        throw new ContactHashMismatchException("Cached email hash of active campaign user id $contact_id is mismatched with the email hash received from active_campaign.");
      }
    }
    \Drupal::cache()->set($cid, $email_hash);
  }

  /**
   * Determine if GDPR cookie acceptance configuration is enabled.
   *
   * Checks the relevant config settings for enabled status as well as
   * a usable contact property to check against.
   *
   * @return bool
   *   TRUE if enabled and configured correctly, FALSE if not.
   */
  private function isPrivacyEnabled(): bool {
    return (
      $this->configuration['privacy']["enabled"]
      &&
      !empty($this->configuration['privacy']["property_name"])
    );
  }

  /**
   * Determine if the contact has flagged that they may be tracked by cookies.
   *
   * Checks the relevant contact property exists and evaluates to TRUE.
   *
   * @return bool
   *   TRUE if contact may be tracked, FALSE if not.
   */
  private function hasUserAllowedTracking(): bool {
    return (
      isset($this->contact->{$this->configuration['privacy']["property_name"]})
      &&
      $this->contact->{$this->configuration['privacy']["property_name"]}
    );

  }

}
