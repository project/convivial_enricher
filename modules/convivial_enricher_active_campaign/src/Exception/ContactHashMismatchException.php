<?php

namespace Drupal\convivial_enricher_active_campaign\Exception;

/**
 * Contact Hash Mismatch Exception.
 *
 * Exception for when an active campaign contact's email hash
 * does not match the one cached for that contact ID.
 * This could imply a spoofing attempt.
 *
 * A scenario, a malicious user registers another user's email address, or
 * very many user's email addresses to a newsletter. Then waits some time for
 * these lists to have been sent to these users.
 *
 * Now the malicious users generate is an /ac/<email hash>/whatever URL for each
 * of these user's email address, loop through each, read the cookie and gather
 * the data that's been collected over time.
 */
class ContactHashMismatchException extends \Exception {}
