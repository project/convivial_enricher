<?php

namespace Drupal\convivial_enricher_active_campaign;

/**
 * Adaptor for APIs that talk to active campaign.
 */
interface ActiveCampaignPhpApiAdaptorInterface {

  /**
   * Set the API Key.
   *
   * Used for authenticating with the active campaign API.
   *
   * @param string $api_key
   *   The API key of the active campaign account we are accessing.
   *
   * @see https://developers.activecampaign.com/reference#authentication
   */
  public function setApiKey(string $api_key);

  /**
   * Set the Active Campaign API BaseURL.
   *
   * @param string $base_url
   *   The BaseURL of the ActiveCampaign REST API.
   *
   * @see https://developers.activecampaign.com/reference#url
   */
  public function setBaseUrl(string $base_url);

  /**
   * Set caching details for the next API call to be performed.
   *
   * The makeApiCall should reset the caching details after performing a
   * request.
   *
   * @param array $cache_details
   *   An array containing two indices, cid and expiry.
   *   Example: [
   *     'cid' => 'my unique cache id',
   *     'expiry' => str_to_time("+1 day"),
   *   ]
   *   The expiry may be one of the following values:
   *   - 0: Do not cache. This is the default.
   *   - CacheBackendInterface::CACHE_PERMANENT: Indicates that the item should
   *     not be removed unless it is deleted explicitly.
   *   - A Unix timestamp: Indicates that the item will be considered invalid
   *     after this time, i.e. it will not be returned by Cache:get() unless
   *     $allow_invalid has been set to TRUE. When the item has expired, it may
   *     be permanently deleted by the garbage collector at any time.
   *
   * @return mixed
   *   Data as retrieved from the cache.
   */
  public function setCacheDetailsForNextApiCall(array $cache_details): void;

  /**
   * Call the Activecampaign API.
   *
   * @param string $endpoint
   *   The part of the api url following api/3 not including the query string.
   * @param array $query_parameters
   *   Array of parameters that will become the query string in the REST call.
   *
   * @return bool|string|void
   *   The result of the REST API call.
   */
  public function makeApiCall($endpoint, array $query_parameters = []);

}
