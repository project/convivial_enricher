<?php

namespace Drupal\convivial_enricher_recombee\Plugin\EnricherDatasource;

use Drupal\convivial_enricher\EnricherDatasourceBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Recombee\RecommApi\Client;
use Recombee\RecommApi\Exceptions\ApiException;
use Recombee\RecommApi\Requests\MergeUsers;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Plugin implementation of the datasource.
 *
 * @EnricherDatasource(
 *   id = "recombee_user_merge",
 *   label = @Translation("Recombee user merge"),
 *   description = @Translation("Recombee datasource for enricher."),
 * )
 */
class RecombeeEnricherDatasource extends EnricherDatasourceBase implements ContainerFactoryPluginInterface {

  /**
   * The Recombee API client.
   *
   * @var \Recombee\RecommApi\Client
   */
  protected $client;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger);
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
    $configuration,
    $plugin_id,
    $plugin_definition,
    $container->get('logger.factory')->get('convivial_enricher_recombee'),
    $container->get('request_stack'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = parent::getSummary();
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'recombee_account' => NULL,
      'recombee_token' => NULL,
      'recombee_cookie' => NULL,
      'recombee_clientid_prefix' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $recombee_link = Link::fromTextAndUrl('Recombee', Url::fromUri('https://admin.recombee.com/'))
      ->toString();

    $form['recombee_account'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Database ID'),
      '#description' => $this->t('The @recombee "API Identifier" value (see "Settings" page for a database).', [
        '@recombee' => $recombee_link,
      ]),
      '#default_value' => $this->configuration['recombee_account'] ?? '',
      '#required' => TRUE,
    ];
    $form['recombee_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private token'),
      '#description' => $this->t('The @recombee "Private token" value (see "Settings" page for a database).', [
        '@recombee' => $recombee_link,
      ]),
      '#default_value' => $this->configuration['recombee_token'] ?? '',
      '#required' => TRUE,
    ];
    $form['recombee_cookie'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookie name'),
      '#description' => $this->t('The recombee cookie name. See the recombee <a href=":module">module</a> for more details.', [
        ':module' => 'https://www.drupal.org/project/recombee',
      ]),
      '#default_value' => $this->configuration['recombee_cookie'] ?? 'RecombeeUserId',
      '#required' => TRUE,
    ];
    $form['recombee_clientid_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recombee clientID prefix'),
      '#description' => $this->t('Recombee clientID prefix'),
      '#default_value' => $this->configuration['recombee_clientid_prefix'] ?? 'ac_',
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['recombee_token'] = $form_state->getValue('recombee_token');
    $this->configuration['recombee_account'] = $form_state->getValue('recombee_account');
    $this->configuration['recombee_cookie'] = $form_state->getValue('recombee_cookie');
    $this->configuration['recombee_clientid_prefix'] = $form_state->getValue('recombee_clientid_prefix');
  }

  /**
   * {@inheritdoc}
   */
  public function processIncomingPath(&$path, $endpoint_path) {
    // Do nothing.
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchAndProcessData($key) {
    $output = [];
    // User merger in recombee.
    $client = $this->getClient();
    $recombee_cookie = $this->configuration['recombee_cookie'] ?? '';
    $recombee_clientid_prefix = $this->configuration['recombee_clientid_prefix'] ?? '';
    $recombee_userid = $this->requestStack->getCurrentRequest()->cookies->get($recombee_cookie);
    $target_user_id = $recombee_clientid_prefix . $key;
    if ($key && $recombee_userid && $target_user_id != $recombee_userid && strpos($recombee_userid, $recombee_clientid_prefix) === FALSE) {
      // Now merge the RecombeeUserId cookie user with the enricher one.
      try {
        $client->send(new MergeUsers($target_user_id, $recombee_userid, ['cascadeCreate' => TRUE]));
        $output['ConvivialEnricherClientId'] = $this->createCookie('ConvivialEnricherClientId', $target_user_id, "+1 year");
      }
      catch (ApiException $e) {
        $error = json_decode($e->getMessage());
        if (!empty($error->message)) {
          $this->logger->error('Recombee API error %code: %message.', [
            '%code' => $error->statusCode,
            '%message' => $error->message,
          ]);
        }
      }
    }
    return $output;
  }

  /**
   * Gets the Recombee API client.
   *
   * @return \Recombee\RecommApi\Client
   *   The Recombee API client.
   */
  private function getClient(): Client {
    // Only if not initialized yet.
    if (empty($this->client)) {
      $account = $this->configuration['recombee_account'] ?? '';
      $token = $this->configuration['recombee_token'] ?? '';

      // Initialize API client.
      $this->client = new Client($account, $token, [
        'serviceName' => 'drupal-convivial-enricher-recombee',
      ]);
    }
    return $this->client;
  }

}
