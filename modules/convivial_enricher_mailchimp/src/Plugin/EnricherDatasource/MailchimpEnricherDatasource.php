<?php

namespace Drupal\convivial_enricher_mailchimp\Plugin\EnricherDatasource;

use Drupal\Component\Serialization\Json;
use Drupal\convivial_enricher\EnricherDatasourceBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Utility\Error;
use Drupal\mailchimp\ClientFactory;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the datasource.
 *
 * @EnricherDatasource(
 *   id = "mailchimp",
 *   label = @Translation("Mailchimp"),
 *   description = @Translation("Mailchimp datasource for enricher."),
 * )
 */
class MailchimpEnricherDatasource extends EnricherDatasourceBase implements ContainerFactoryPluginInterface {

  /**
   * Mailchimp API client.
   *
   * @var null|\Mailchimp\Mailchimp
   */
  private $mailchimpApiClient;

  /**
   * An object representing a Mailchimp list member.
   *
   * @var object
   */
  private $listMember;

  /**
   * The endpoint Path.
   *
   * @var string
   */
  protected $endpointPath;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, ClientFactory $mailchimp_client_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger);
    $this->mailchimpApiClient = $mailchimp_client_factory->getByClassNameOrNull('MailchimpLists');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('convivial_enricher'),
      $container->get('mailchimp.client_factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = parent::getSummary();
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'mailchimp_list_id' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['mailchimp_list_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mailchimp list ID'),
      '#description' => $this->t('The Mailchimp list ID.'),
      '#default_value' => $this->configuration['mailchimp_list_id'],
      '#required' => TRUE,
    ];

    $help = nl2br($this->t("Enter a list of allowed properties to be retrieved, one per line. All other properties are ignored.
The '*' character is a wildcard. All entries are treated as a shell wildcard pattern.
Examples:
  tags matches every property name equals to tags.
  tags* matches every property name that starts with tags.
  *tags matches every property name that ends tags.
  *tags* matches every property name that contains tags.

Other shell wildcards such as ., ?, !, [] are honored for advanced users."));

    $form['allowed_contact_properties'] = [
      '#title' => $this->t('Contact Properties'),
      '#type' => 'textarea',
      '#default_value' => $this->configuration['allowed_contact_properties'] ?? 'tags',
      '#required' => TRUE,
      '#description' => $help,
    ];

    $help = nl2br($this->t("Enter a list of allowed tags namespaces to be retrieved, one per line. All other tags are ignored.
The '*' character is a wildcard. All entries are treated as a shell wildcard pattern.
Examples:
  campaign* matches every tag that starts with the namespace campaign.
  *campaign matches every tag that ends the namespace campaign.
  *campaign* matches every tag that contains the namespace campaign.

Important: On Mailchimp each tag should have a '/' separating the namespace from the tag name, example 'campaign/recurring-customer'."));

    $form['allowed_contact_tags'] = [
      '#title' => $this->t('Contact Tags'),
      '#type' => 'textarea',
      '#default_value' => $this->configuration['allowed_contact_tags'] ?? '',
      '#description' => $help,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['mailchimp_list_id'] = $form_state->getValue('mailchimp_list_id');
    $this->configuration['allowed_contact_properties'] = $form_state->getValue('allowed_contact_properties');
    $this->configuration['allowed_contact_tags'] = $form_state->getValue('allowed_contact_tags');
  }

  /**
   * {@inheritdoc}
   *
   * Take the incoming path and process it if necessary.
   *
   * Sample use case: converting /mypath/<token>/return/to/path, as
   * drupal does not allow / is parameters, we would need to process this url
   * to encode it differently and then drupal can digest the new path.
   *
   * This is designed to be called from our
   * \Drupal\convivial_enricher\PathProcessor\InboundPathProcessor
   */
  public function processIncomingPath(&$path, $endpointPath) {
    $this->endpointPath = $endpointPath;
    return $this->convertPathToDataEncodedString($path);
  }

  /**
   * Modify the incoming path/convert incoming path to something usable.
   *
   * @param string $path
   *   The incoming path string. eg: The /my/path portion of the incoming URL.
   *
   * @return bool
   *   TRUE if path was converted by this method, false if not.
   */
  private function convertPathToDataEncodedString(string &$path): string {
    if ($this->pathRequiresConversion($path)) {
      $token     = $this->getTokenFromPath($path);
      $return_to = $this->getReturnToPortionOfPath($path);

      $encoded_data_string = base64_encode("return_to={$return_to}&token={$token}");

      $path = '/' . $this->endpointPath . "/data:$encoded_data_string";
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get the token from the provided path string.
   *
   * @param string $path
   *   The incoming path string. eg: The /my/path portion of the incoming URL.
   *
   * @return string
   *   The token value calculated from the supplied path string.
   */
  private function getTokenFromPath(string $path) {
    $token = preg_replace('|^\/' . $this->endpointPath . '\/(.*?)\/.*$|', '$1', $path);
    return $token;
  }

  /**
   * Get the return_to parameter encoded in the path.
   *
   * @param string $path
   *   The incoming path string. eg: The /my/path portion of the incoming URL.
   *
   * @return string
   *   The return_to value calculated from the supplied path string.
   */
  private function getReturnToPortionOfPath(string $path): string {
    $return_to = preg_replace('|^\/' . $this->endpointPath . '\/.*?(\/.*)$|', '$1', $path);
    return $return_to;
  }

  /**
   * Determine if the format of this incoming path requires conversion.
   *
   * @param string $path
   *   The incoming path string. eg: The /my/path portion of the incoming URL.
   *
   * @return bool
   *   TRUE if path requires conversion to a drupal usable format.
   */
  private function pathRequiresConversion(string $path): bool {
    return $this->pathIsAnEndpointPath($path, $this->endpointPath) && $this->pathDoesNotHaveLoadedData($path, $this->endpointPath);
  }

  /**
   * Analyze this path to determine if it contains encoded data we can read.
   *
   * @param string $path
   *   The incoming path string. eg: The /my/path portion of the incoming URL.
   *
   * @return bool
   *   TRUE if path contains encoded data, FALSE if not.
   */
  private function pathDoesNotHaveLoadedData(string $path): bool {
    return strpos($path, "/$this->endpointPath/data:") !== 0;
  }

  /**
   * Determine if the given path is our endpoint.
   *
   * Compare the path to the endpointPath to determine if this is one of
   * our defined endpoints.
   *
   * @param string $path
   *   The incoming path string. eg: The /my/path portion of the incoming URL.
   *
   * @return bool
   *   TRUE if path is a processable endpoint.
   */
  private function pathIsAnEndpointPath(string $path): bool {
    return strpos($path, "/$this->endpointPath/") === 0;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchAndProcessData($key) {
    $output = [];
    $return_cookies = [];
    $allowed_properties = $this->getAllowedContactProperties();
    $allowed_tags = $this->getAllowedContactTags();

    try {
      $list_id = $this->configuration['mailchimp_list_id'];
      $this->listMember = $this->getListMemberInfoById($list_id, $key);
    }
    catch (\Exception $exception) {
      $variables = Error::decodeException($exception);
      $this->logger->warning('%type: @message in %function (line %line of %file).', $variables);
    }

    // Create the cookies of all the output whitelisted fields.
    foreach ($this->listMember->members as $member) {
      $fields = (array) $member;
      $fields = $this->filterKeyValueSetOnAcceptList($fields, $allowed_properties);
      foreach ($fields as $field_name => $field_value) {

        if ($field_name == 'tags') {
          $tags = $this->getContactTags($field_value);
          $tags = $this->filterKeyValueSetOnAcceptList($tags, $allowed_tags);

          foreach ($tags as $name => $value) {
            $return_cookies[] = $this->createCookie($name, $value);
          }
          continue;
        }
        elseif (is_array($field_value) || is_object($field_value)) {
          $field_value = Json::encode($field_value);
        }

        $return_cookies[] = $this->createCookie($field_name, $field_value);
      }
    }
    return $return_cookies;
  }

  /**
   * Gets information about a member of a Mailchimp list.
   *
   * @param string $list_id
   *   The ID of the list.
   * @param string $uniqid
   *   The member's unique ID (UNIQID).
   *
   * @return null|object
   *   An object representing a Mailchimp list member or NULL if not found.
   */
  protected function getListMemberInfoById($list_id, $uniqid) {
    if (isset($this->mailchimpApiClient)) {
      return $this->mailchimpApiClient->getMemberInfoById($list_id, $uniqid);
    }
  }

  /**
   * Return the $key_values filtered on key by the patterns in the allow_list.
   *
   * @param array $key_values
   *   An array of keys and values to filter.
   * @param array $allow_list
   *   A list of patterns suitable for fnmatch() used to filter by key.
   *
   * @return array
   *   A filtered array of keys and values.
   *
   *   With keys matching patterns in allow_list removed.
   */
  private function filterKeyValueSetOnAcceptList(array $key_values, array $allow_list): array {

    foreach ($key_values as $key => $value) {

      $tag_allowed = FALSE;
      foreach ($allow_list as $pattern) {
        // Ignore PHPCS Warning for "The use of function fnmatch() is
        // discouraged"
        // According to PHP's documentation
        // "For now, this function is not available on non-POSIX compliant
        // systems except Windows."
        // - https://www.php.net/manual/en/function.fnmatch.php
        // Running this on a non-posix compliant system and not windows
        // is very unlikely.
        // @codingStandardsIgnoreStart
        if (is_string($key) && fnmatch(trim($pattern), $key)) {
          // @codingStandardsIgnoreEnd
          $tag_allowed = TRUE;
        }
      }

      if (!$tag_allowed) {
        unset($key_values[$key]);
      }

    }

    return $key_values ?? [];
  }

  /**
   * Gets a list of whitelisted contact properties.
   *
   * @return array
   *   An array with all the allowed contact properties.
   */
  protected function getAllowedContactProperties() {
    return explode(PHP_EOL, $this->configuration['allowed_contact_properties']);
  }

  /**
   * Gets a list of whitelisted contact tags.
   *
   * @return array
   *   An array with all the allowed contact tags.
   */
  protected function getAllowedContactTags() {
    return explode(PHP_EOL, $this->configuration['allowed_contact_tags']);
  }

  /**
   * Gets a list of contact tags.
   *
   * @return array
   *   An array with all the contact tags.
   */
  protected function getContactTags($raw_tags) {
    $tags = [];

    foreach ($raw_tags as $raw_tag) {
      if (str_contains($raw_tag->name, '/')) {
        $tag = explode('/', $raw_tag->name);
        $tags[$tag[0]] = $tag[1];
      }
    }
    return $tags;
  }

}
